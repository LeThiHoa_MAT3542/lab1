-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2022 at 04:41 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dmkhoa`
--

-- --------------------------------------------------------

--
-- Table structure for table `dmkhoa`
--

CREATE TABLE `dmkhoa` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dmkhoa`
--

INSERT INTO `dmkhoa` (`MaKH`, `TenKhoa`) VALUES
('QT111', 'Công nghệ thông tin'),
('QT112', 'Khoa học dữ liệu'),
('QT113', 'Quản lý đất đai'),
('QT114', 'Công nghệ sinh học'),
('QT115', 'Vật lý học');

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `sinhvien` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) DEFAULT NULL,
  `TenSV` varchar(15) DEFAULT NULL,
  `GioiTinh` char(1) DEFAULT NULL,
  `NgaySinh` datetime DEFAULT NULL,
  `NoiSinh` varchar(50) DEFAULT NULL,
  `DiaChi` varchar(50) DEFAULT NULL,
  `MaKH` varchar(6) NOT NULL,
  `HocBong` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES
('190004', ' Le Thi', 'Hoa', NULL, '2002-02-17 10:15:10', 'Ninh Binh', 'Trang An', 'QH112', 0),
('190005', ' Pham Thi', 'Anh', NULL, '2001-05-16 13:15:15', 'Ha Noi', 'Thanh Oai', 'QH181', 0),
('190052', ' Do Thanh', 'Nhunh', NULL, '2001-06-01 13:15:15', 'Ha Noi', 'Hang Dong', 'QH811', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dmkhoa`
--
ALTER TABLE `dmkhoa`
  ADD PRIMARY KEY (`MaKH`);

--
-- Indexes for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`MaSV`),
  ADD KEY `fk_ma_kh` (`MaKH`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
